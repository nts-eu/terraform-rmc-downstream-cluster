variable "rmc_bearer_token" {
  type      = string
  sensitive = true
  default = "token-khz9q:59v52rj55x2rffp7spjcqqds8gbhnkhtjxr8t6jvdkd4w27k85jczx"
}

variable "vsphere_password" {
  type      = string
  sensitive = true
}

variable "node" {
  description = "Properties for MachinePool node types"
  type = object({
    ctl_plane = map(any)
    worker    = map(any)
  })
}

variable "rke2" {
  type = map(any)
}

variable "vsphere" {
  type = map(any)
}

variable "rmc" {
  type = map(any)
}