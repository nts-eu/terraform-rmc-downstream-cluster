resource "rancher2_machine_config_v2" "nodes" {
  for_each      = var.node
  generate_name = replace(each.value.name, "_", "-")

  vsphere_config {
    cfgparam      = ["disk.enableUUID=TRUE"]
    clone_from    = var.vsphere.template
    cloud_config  = templatefile("${path.cwd}/files/user_data_${each.key}.tftmpl", {})
    cpu_count     = each.value.vcpu
    creation_type = "template"
    datacenter    = var.vsphere.datacenter
    datastore     = var.vsphere.datastore
    disk_size     = each.value.hdd_capacity
    memory_size   = each.value.vram
    network       = [var.vsphere.portgroup]
    vcenter       = var.vsphere.server
    pool          = var.vsphere.pool == "" ? "/${var.vsphere.datacenter}/host/${var.vsphere.cluster}/Resources" : "/${var.vsphere.datacenter}/host/${var.vsphere.cluster}/Resources/${var.vsphere.pool}"
    vapp_transport = "com.vmware.guestInfo"
    vapp_ip_protocol = "IPv4"
    vapp_ip_allocation_policy = "fixedAllocated"
    vapp_property = [
      "guestinfo.interface.0.ip.0.address=ip:${var.vsphere.portgroup}",
      "guestinfo.interface.0.ip.0.netmask=$${netmask:${var.vsphere.portgroup}}",
      "guestinfo.interface.0.route.0.gateway=$${gateway:${var.vsphere.portgroup}}",
      "guestinfo.dns.servers=$${dns:${var.vsphere.portgroup}}"
    ]
  }
}

resource "rancher2_cluster_v2" "rke2" {
  kubernetes_version = var.rke2.version
  name               = var.rke2.cluster_name
  rke_config {
    chart_values = <<EOF
      rancher-vsphere-cpi:
        vCenter:
          credentialsSecret:
            generate: true
            name: vsphere-cpi-creds
          datacenters: ${var.vsphere.datacenter}
          host: ${var.vsphere.server}
          insecureFlag: true
          password: ${var.vsphere_password}
          port: 443
          username: ${var.vsphere.user}
      rancher-vsphere-csi:
        storageClass:
          storagePolicyName: ${var.vsphere.storagePolicy}
        vCenter:
          datacenters: ${var.vsphere.datacenter}
          host: ${var.vsphere.server}
          password: ${var.vsphere_password}
          username: ${var.vsphere.user}
    EOF

    machine_global_config = <<EOF
      cni: calico
      etcd-arg: [ "experimental-initial-corrupt-check=true" ] # Can be removed with etcd v3.6, which will enable corruption check by default (see: https://github.com/etcd-io/etcd/issues/13766)
      kube-apiserver-arg: [ "audit-log-mode=blocking-strict","enable-admission-plugins=AlwaysPullImages,NodeRestriction","tls-min-version=VersionTLS13" ]
      kube-controller-manager-arg: [ "terminated-pod-gc-threshold=10","tls-min-version=VersionTLS13" ]
      kube-scheduler-arg: [ "tls-min-version=VersionTLS13" ]
      kubelet-arg: [ "cgroup-driver=systemd","event-qps=0","make-iptables-util-chains=true","tls-min-version=VersionTLS13" ]
    EOF

    dynamic "machine_pools" {
      for_each = var.node
      content {
        cloud_credential_secret_name = data.rancher2_cloud_credential.auth.id
        control_plane_role           = machine_pools.key == "ctl_plane" ? true : false
        etcd_role                    = machine_pools.key == "ctl_plane" ? true : false
        name                         = machine_pools.value.name
        quantity                     = machine_pools.value.quantity
        worker_role                  = machine_pools.key != "ctl_plane" ? true : false

        machine_config {
          kind = rancher2_machine_config_v2.nodes[machine_pools.key].kind
          name = replace(rancher2_machine_config_v2.nodes[machine_pools.key].name, "_", "-")
        }
      }
    }

    machine_selector_config {
      config = {
        cloud-provider-name     = "rancher-vsphere"
        profile                 = "cis-1.6"
        protect-kernel-defaults = true # Required to install RKE2 with CIS Profile enabled
      }
    }
  }
}
