node = {
  ctl_plane = {
    hdd_capacity = 40960,
    name         = "ctl-plane",
    quantity     = 3,
    vcpu         = 4,
    vram         = 4096
  }
  worker = {
    hdd_capacity = 40960,
    name         = "worker",
    quantity     = 3,
    vcpu         = 4,
    vram         = 8192
  }
}
vsphere = {
  server        = "ibkvc01.ntslab.loc",
  user          = "administrator@vsphere.local",
  template      = "ubuntu-2204-cloud-image",
  datacenter    = "IDT-NET",
  cluster       = "NET-B200-M4"
  datastore     = "NET_SSD_DS03",
  portgroup     = "DPG-IDTLAB-86",
  pool          = ""
  storagePolicy = "test-sp",
}

rke2 = {
  version      = "v1.24.9+rke2r2",
  cluster_name = "kof-test"
}

rmc = {
  api_url = "https://rke2-test.ntslab.loc",
  cloud_credential = "vsphere"
}