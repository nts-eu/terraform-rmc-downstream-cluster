terraform {
  required_providers {
    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.25.0"
    }
  }
}

provider "rancher2" {
  api_url   = var.rmc.api_url
  token_key = var.rmc_bearer_token
  insecure  = true
}
