node = {
  ctl_plane = {
    hdd_capacity = 40960,
    name         = "ctl-plane",
    quantity     = 3,
    vcpu         = 4,
    vram         = 4096
  }
  worker = {
    hdd_capacity = 40960,
    name         = "worker",
    quantity     = 3,
    vcpu         = 4,
    vram         = 8192
  }
}
vsphere = {
  server = "ibkvc01.ntslab.loc",
  user = "administrator@vsphere.local",
  template = "ubuntu-2204-cloud-image",
  datacenter = "IDT-NET",
  datastore = "NET_SAS_DS02",
  network = "DPG-IDTLAB-86",
  pool = ""
  storagePolicy = "",
}

rke2 = {
  version = "",
  cluster_name = "dkofler-test"
}
